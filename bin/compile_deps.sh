#!/usr/bin/env bash
set -x
pip-compile requirements/development.in
pip-compile requirements/production.in
