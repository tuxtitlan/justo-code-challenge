from django.contrib.auth.models import User
from django.db import models

from djchoices import DjangoChoices, ChoiceItem

from tools.exceptions import StateMachineException


class Hitman(User):
    manager = models.ForeignKey(
        "self", on_delete=models.SET_NULL, blank=True, null=True, related_name="managed"
    )
    description = models.TextField()
    is_manager = models.BooleanField(default=False)

    class Meta:
        verbose_name = "hitman"
        verbose_name_plural = "hitmen"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._original_is_active = self.is_active

    @property
    def is_boss(self):
        return self.pk == 1

    def get_full_name(self):
        return f"{self.first_name} {self.last_name}"

    def save(self, *args, **kwargs):
        if not self._original_is_active and self.is_active:
            raise StateMachineException("Inactive hitman cannot be activated again")

        super().save(*args, **kwargs)


class HitStateChoices(DjangoChoices):
    Assigned = ChoiceItem("A")
    Failed = ChoiceItem("F")
    Completed = ChoiceItem("C")


class Hit(models.Model):

    assignee = models.ForeignKey(Hitman, on_delete=models.PROTECT, related_name="hits")
    creator = models.ForeignKey(
        Hitman, on_delete=models.PROTECT, related_name="created"
    )
    state = models.CharField(
        max_length=1, choices=HitStateChoices.choices, default=HitStateChoices.Assigned
    )
    target = models.TextField()
    description = models.TextField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._original_state = self.state

    def save(self, *args, **kwargs):
        if (
            self._original_state in [HitStateChoices.Failed, HitStateChoices.Completed]
            and self.state == HitStateChoices.Assigned
        ):
            raise StateMachineException("Closed hits cannot be reopened")

        super().save(*args, **kwargs)
