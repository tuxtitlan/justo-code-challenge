from unittest import skip

from django.test import TestCase

from hitman.models import Hit, HitStateChoices
from hitman.tests.factories import HitmanFactory, HitFactory
from tools.exceptions import StateMachineException


class HitmanModelTests(TestCase):
    def setUp(self):
        self.boss = HitmanFactory()

    def test_first_hitman_is_the_boss(self):
        self.assertTrue(self.boss.is_boss)

    def test_deactivated_hitman_cannot_be_reactivated(self):
        hitman = HitmanFactory(is_active=False)

        with self.assertRaises(StateMachineException):
            hitman.is_active = True
            hitman.save()

        hitman.refresh_from_db()
        self.assertFalse(hitman.is_active)

    def test_hitman_can_be_managed_by_other_hitman(self):
        hitman = HitmanFactory()
        self.assertIsNone(hitman.manager)
        self.boss.managed.add(hitman)
        hitman.refresh_from_db()
        self.assertEqual(hitman.manager, self.boss)


class HitModelTest(TestCase):
    def test_closed_hit_cannot_be_reopened(self):
        failed_hit = HitFactory(state=HitStateChoices.Failed)
        completed_hit = HitFactory(state=HitStateChoices.Completed)

        with self.assertRaises(StateMachineException):
            failed_hit.state = HitStateChoices.Assigned
            failed_hit.save()

        with self.assertRaises(StateMachineException):
            completed_hit.state = HitStateChoices.Assigned
            completed_hit.save()

    def test_hits_can_be_reassigned(self):
        hit = HitFactory()
        new_hitman = HitmanFactory()
        old_hitman = hit.assignee

        hit.assignee = new_hitman
        hit.save()

        hit.refresh_from_db()
        self.assertIsNot(hit.assignee, old_hitman)

    @skip(
        "Per single-responsibility principle, this should not be handled by the model"
    )
    def test_hits_should_not_be_assigned_to_inactive_hitmans(self):
        hit = HitFactory()
        dead_hitman = HitmanFactory(is_active=False)
        old_hitman = hit.assignee

        hit.assignee = dead_hitman
        hit.save()

        hit.refresh_from_db()
        self.assertIsNot(hit.assignee, old_hitman)
