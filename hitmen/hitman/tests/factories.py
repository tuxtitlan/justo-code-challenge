from factory import fuzzy, Faker, SubFactory
from factory.django import DjangoModelFactory

from hitman.models import Hit, Hitman, HitStateChoices


class HitmanFactory(DjangoModelFactory):
    first_name = Faker("first_name")
    last_name = Faker("first_name")
    email = Faker("email")
    username = Faker("user_name")
    description = Faker("sentence")
    is_manager = False
    is_active = True

    class Meta:
        model = Hitman


class HitFactory(DjangoModelFactory):
    assignee = SubFactory(HitmanFactory)
    creator = SubFactory(HitmanFactory)
    state = HitStateChoices.Assigned
    target = Faker("name")
    description = Faker("sentence")

    class Meta:
        model = Hit
